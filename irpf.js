var registros = [];

document.querySelector('#cadastrar').addEventListener('click', () => {

    var funcionario = document.querySelector('#funcionario').value;
    var cargo = document.querySelector('#cargo').value;
    var salarioBruto = document.querySelector('#salario').value;

    let iprf = calcularIprf(salarioBruto);

    registros.push({
        "cod": "",
        "funcionario": funcionario,
        "cargo": cargo,
        "salarioB": salarioBruto,
        "iprf": iprf,
        "salarioL": salarioBruto - iprf
    })
});

function calcularIprf(salario) {
    let imposto;

    const baseIsenta = Number('1.903,98');

    switch (salario) {
        case salario == baseIsenta:
            imposto = 0;
            break;
        case salario >= '1.903,99' && salario <= '2.826,65':
            imposto = (salario - baseIsenta) * 0.075;
            break;
        case salario >= 2.826, 66 && salario <= 3.751, 05:
            imposto = (2.826, 65 - 1.903, 99) * 0.075 + (salario - 2.826, 65) * 0.15;
            break;
        case salario >= 3.751, 06 && salario <= 4.664, 68:
            imposto = (2.826, 65 - 1.903, 99) * 0.075 + (3.751, 05 - 2.826, 66) * 0.15 + (salario - 3.751, 05) * 0.225;
            break;
        case salario > 4.664, 68:
            imposto = (2.826, 65 - 1.903, 99) * 0.075 + (3.751, 05 - 2.826, 66) * 0.15 + (4.664, 68 - 3.751, 05) * 0.225(salario - 4.664, 68) * 0.275;
            break;
    }
    return imposto;
}


document.querySelector('#finalizar').addEventListener('click', () => {
    document.querySelector('#form').innerHTML = '';
    document.querySelector('#table').removeAttribute('style', 'display: none');
    gerarTabela();
})

function gerarTabela() {
    let tbody = document.querySelector('#tbody');

    registros.forEach((funcionario, index) => {
        let tr = document.createElement('tr');
        tr.setAttribute('scope', 'row');

        tdCod = document.createElement('td');
        tdFuncionario = document.createElement('td');
        tdCargo = document.createElement('td');
        tdSalarioB = document.createElement('td');
        tdIprf = document.createElement('td');
        tdSalarioL = document.createElement('td');

        tdCod.innerHTML = index + 1;
        tdCod.innerHTML = funcionario.cod;
        tdFuncionario.innerHTML = funcionario.funcionario;
        tdCargo.innerHTML = funcionario.cargo;
        tdSalarioB.innerHTML = funcionario.salarioB;
        tdIprf.innerHTML = funcionario.imposto;
        tdSalarioL.innerHTML = funcionario.salarioL;

        tbody.appendChild(tr)
    });
}